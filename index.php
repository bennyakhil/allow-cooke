<!DOCTYPE html>
<html itemscope itemtype="https://schema.org/QAPage" class="html__responsive">
   <head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="consent-action.js"></script>
      <link rel="stylesheet" type="text/css" href="https://cdn.sstatic.net/Shared/stacks.css?v=f5e0a087a500">
      <script>
         StackExchange.init({"locale":"en","serverTime":1617946097,"routeName":"Questions/Show","stackAuthUrl":"https://stackauth.com","networkMetaHostname":"meta.stackexchange.com","site":{"name":"Stack Overflow","description":"Q&A for professional and enthusiast programmers","isNoticesTabEnabled":true,"enableNewTagCreationWarning":true,"insertSpaceAfterNameTabCompletion":false,"id":1,"cookieDomain":".stackoverflow.com","childUrl":"https://meta.stackoverflow.com","styleCodeWithHighlightjs":true,"negativeVoteScoreFloor":null,"enableSocialMediaInSharePopup":true,"protocol":"https"},"user":{"fkey":"24f14e2b901aa4f74cb9cca224c1358d468e9cf47807540be5934b829495cc04","tid":"93a777aa-1b6b-e44a-cc55-e184d8d02a0b","rep":0,"isAnonymous":true,"isAnonymousNetworkWide":true},"events":{"postType":{"question":1},"postEditionSection":{"title":1,"body":2,"tags":3}},"story":{"minCompleteBodyLength":75,"likedTagsMaxLength":300,"dislikedTagsMaxLength":300},"jobPreferences":{"maxNumDeveloperRoles":2,"maxNumIndustries":4},"svgIconPath":"https://cdn.sstatic.net/Img/stacks-icons","svgIconHash":"7eb510ec72e5"}, {"userProfile":{"openGraphAPIKey":"4a307e43-b625-49bb-af15-ffadf2bda017"},"userMessaging":{"showNewFeatureNotice":true},"tags":{},"subscriptions":{"defaultBasicMaxTrueUpSeats":250,"defaultFreemiumMaxTrueUpSeats":50,"defaultMaxTrueUpSeats":1000},"snippets":{"renderDomain":"stacksnippets.net","snippetsEnabled":true},"slack":{"sidebarAdDismissCookie":"slack-sidebar-ad","sidebarAdDismissCookieExpirationDays":60.0},"site":{"allowImageUploads":true,"enableImgurHttps":true,"enableUserHovercards":true,"forceHttpsImages":true,"styleCode":true},"questions":{"enableQuestionTitleLengthLiveWarning":true,"maxTitleSize":150,"questionTitleLengthStartLiveWarningChars":50},"intercom":{"appId":"inf0secd","hostBaseUrl":"https://stacksnippets.net"},"paths":{},"monitoring":{"clientTimingsAbsoluteTimeout":30000,"clientTimingsDebounceTimeout":1000},"mentions":{"maxNumUsersInDropdown":50},"markdown":{"enableTables":true},"legal":{"oneTrustConfigId":"c3d9f1e3-55f3-4eba-b268-46cee4c6789c"},"flags":{"allowRetractingCommentFlags":true,"allowRetractingFlags":true},"comments":{},"accounts":{"currentPasswordRequiredForChangingStackIdPassword":true}});
         StackExchange.using.setCacheBreakers({"js/adops.en.js":"22a9bd59b1e9","js/ask.en.js":"91b4450eec6e","js/begin-edit-event.en.js":"cb9965ad8784","js/copy-transpiled.en.js":"9da14278df5c","js/cm.en.js":"19f77005eb72","js/events.en.js":"d31c2b9b13a7","js/explore-qlist.en.js":"d046fe5d0680","js/full-anon.en.js":"1d8d4a3d4393","js/full.en.js":"c5a90126678c","js/help.en.js":"76e2886f2122","js/highlightjs-loader.en.js":"ef72d7fc4d42","js/inline-tag-editing.en.js":"1b88a7bb274f","js/keyboard-shortcuts.en.js":"5c47fbf3d7f6","js/markdown-it-loader.en.js":"b43303c93c4e","js/modElections.en.js":"e5e2c4e4dd6d","js/mobile.en.js":"9400e80a5686","js/moderator.en.js":"457e1ae9813c","js/postCollections-transpiled.en.js":"59995faab763","js/post-validation.en.js":"9f6a865d70a8","js/prettify-full.en.js":"715a2345e97f","js/question-editor.en.js":"","js/review.en.js":"8cf990b834d8","js/review-v2-transpiled.en.js":"f43bf8b652a6","js/revisions.en.js":"b4e68bf9aff9","js/stacks-editor.en.js":"7055e33fb671","js/tageditor.en.js":"113f45a6e92c","js/tageditornew.en.js":"e7c1e520fea8","js/tagsuggestions.en.js":"4d2d70ee1e16","js/unlimited-transpiled.en.js":"5210638beeaf","js/wmd.en.js":"c7828effeab7","js/snippet-javascript-codemirror.en.js":"06008e63096a"});
         StackExchange.using("gps", function() {
              StackExchange.gps.init(false);
         });
      </script>
   </head>
  
      you can load file only after clicking the allow js button

      <div class="ff-sans ps-fixed z-nav-fixed ws4 sm:w-auto p32 bg-black-750 fc-white bar-lg b16 l16 r16 js-consent-banner">
         <p class="fs-body2 fw-bold mb4">
            Your privacy
         </p>
         <p class="mb16 s-anchors s-anchors__inherit s-anchors__underlined">
            By clicking “Accept all cookies”, you agree Stack Exchange can store cookies on your device and disclose information in accordance with our <a href="https://stackoverflow.com/legal/cookie-policy">Cookie Policy</a>.
         </p>
         <div class="grid gs8 ai-stretch fd-column sm:fd-row">
            <button class="grid--cell s-btn s-btn__primary js-accept-cookies js-consent-banner-hide">
            Accept all cookies
            </button>
         </div>
      </div>
      <div id="onetrust-consent-sdk" class="d-none"></div>
      <div id="onetrust-banner-sdk" data-controller="s-modal"></div>
      <div id="ot-pc-content" class="d-none"></div>
      <div id="onetrust-style" class="d-none">&nbsp;</div>
      <div class="d-none js-consent-banner-version" data-consent-banner-version="baseline"></div>
   </body>
</html>